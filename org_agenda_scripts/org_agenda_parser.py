#!/usr/bin/env python3
"""A class that reads and parses the org agenda."""
import os
import io
import datetime
import re


class OrgAgendaParser:
    """A class that reads and parses the org agenda."""

    def __init__(self, load_lisp):
        """Initialize OrgAgendaParser, the load_lisp variable is basically what
        lisp to load to see where are the users org agenda files. If you are
        using doom emacs then set this to a path variable that is
        ~/.doom.d/custom.el"""
        self.load_lisp = load_lisp

    def get_org_agenda_info(self, info_type_char):
        """Get the org agenda todos."""
        return os.popen(
            f"emacs -batch -l ~/.doom.d/custom.el -eval '(org-batch-agenda \"{info_type_char}\")'"
        ).read()

    def get_org_agenda_todos(self):
        """Get the org agenda todos."""
        return self.get_org_agenda_info("t")

    def get_org_weekly_agenda(self):
        """Get the org agenda todos."""
        return self.get_org_agenda_info("a")

    def get_org_daily_agenda(self):
        """Get the org agenda todos."""
        return os.popen(
            "emacs -batch -l ~/.doom.d/custom.el -eval '(org-batch-agenda \"a\" org-agenda-span (quote day)))'"
        ).read()

    def get_daily_scheduled_tasks(self):
        """Get daily todos"""
        agenda = self.get_org_daily_agenda()
        scheduled_lines = []
        for line in io.StringIO(agenda):
            if "Scheduled" in line:
                scheduled_lines.append(line.rstrip())

        return scheduled_lines

    def get_today_deadlines(self):
        """Get today's deadlines"""
        agenda = self.get_org_daily_agenda()
        deadline_lines = []
        for line in io.StringIO(agenda):
            if "Deadline" in line:
                deadline_lines.append(line.rstrip())

        return deadline_lines

    def get_today_scheduled_timed_tasks(self):
        """Get today's incomplete scheduled tasks that have a specific time attached to them"""
        tasks_today_arr = self.get_daily_scheduled_tasks()
        # filter out tasks that don't have a time
        p = re.compile(r"\b\d{2}:\d{2}\b")
        tasks_today_arr = list(filter(p.search, tasks_today_arr))
        tasks = []
        for task in tasks_today_arr:
            task_time = re.search(r"\b\d{2}:\d{2}\b", task).group(0)
            # get task after the todo keyword
            task = task[task.find("TODO")+5:]
            tasks.append((task_time, task))
        return tasks 
