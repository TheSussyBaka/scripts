import notify2
from org_agenda_parser import OrgAgendaParser
import datetime
import os

parser = OrgAgendaParser("~/.doom.d/custom.el")


def notify_timely_tasks(time_bound=0):
    print("Checking for tasks...")
    # TODO implement time_bound to notify within a certain time frame
    """Notify the user of their tasks during the time they occour"""
    # Get current time in HH:MM format
    current_time = datetime.datetime.now().strftime("%H:%M")
    # Get the tasks that are scheduled for today
    today_timed_tasks = parser.get_today_scheduled_timed_tasks()
    curr_file_dir = os.path.dirname(__file__)
    notification_img = os.path.join(curr_file_dir, "image.png")
    audio = os.path.join(curr_file_dir, "audio.mp3")

    for task in today_timed_tasks:
        time = task[0]
        task = task[1]
        if time == current_time:
            notify2.init("org-agenda-notifier")
            n = notify2.Notification(
                "New Task for " + time,
                task + "\n\nDo it for mugi",
                icon=notification_img,
            )
            n.set_urgency(notify2.URGENCY_CRITICAL)
            n.show()
            print(task)
            print("Notification Sent")
            # TODO consider using subprocess.popen instead
            os.system("/usr/bin/mplayer " + audio)


notify_timely_tasks()
